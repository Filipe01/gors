# Define BGP timer variables
$KEEP_ALIVE_TIMER = 10
$HOLD_TIMER = 30

# Define router names and corresponding BGP neighbor configurations
$BGP_CONFIGS = @{
    "setup1-router1-1" = @{
        "AS" = 65001;
        "Neighbors" = @("172.21.0.2", "172.27.0.7", "172.29.0.8")
    }
    "setup1-router2-1" = @{
        "AS" = 65002;
        "Neighbors" = @("172.21.0.11", "172.22.0.3")
    }
    "setup1-router3-1" = @{
        "AS" = 65003;
        "Neighbors" = @("172.22.0.2", "172.24.0.4")
    }
    "setup1-router4-1" = @{
        "AS" = 65004;
        "Neighbors" = @("172.24.0.3", "172.25.0.5")
    }
    "setup1-router5-1" = @{
        "AS" = 65005;
        "Neighbors" = @("172.25.0.4", "172.26.0.6")
    }
    "setup1-router6-1" = @{
        "AS" = 65006;
        "Neighbors" = @("172.26.0.5", "172.28.0.7", "172.30.0.8")
    }
    "setup1-router7-1" = @{
        "AS" = 65007;
        "Neighbors" = @("172.27.0.11", "172.28.0.6")
    }
    "setup1-router8-1" = @{
        "AS" = 65008;
        "Neighbors" = @("172.29.0.11", "172.30.0.6")
    }
}

# Define computer container names and their gateway IPs
$COMPUTER_CONFIGS = @{
    "setup1-computer1-1" = "172.20.0.11";
    "setup1-computer2-1" = "172.23.0.3";
    "setup1-computer3-1" = "172.31.0.6"
}

# Set default gateway for each computer
foreach ($COMPUTER in $COMPUTER_CONFIGS.Keys) {
    $GATEWAY_IP = $COMPUTER_CONFIGS[$COMPUTER]
    Write-Host "Setting default gateway for $COMPUTER to $GATEWAY_IP..."
    docker exec $COMPUTER bash -c "ip route delete default"
    docker exec $COMPUTER bash -c "ip route add default via $GATEWAY_IP"
    Write-Host "Default gateway set for $COMPUTER`n"
}

# Configure BGP timers for routers
foreach ($ROUTER in $BGP_CONFIGS.Keys) {
    $BGP_AS = $BGP_CONFIGS[$ROUTER]["AS"]
    $NEIGHBORS = $BGP_CONFIGS[$ROUTER]["Neighbors"]

    Write-Host "Configuring BGP for $ROUTER with AS $BGP_AS..."

    foreach ($NEIGHBOR in $NEIGHBORS) {
        Write-Host "Setting BGP neighbor $NEIGHBOR timers to $KEEP_ALIVE_TIMER $HOLD_TIMER..."
        docker exec $ROUTER vtysh -c "configure terminal" `
                                  -c "router bgp $BGP_AS" `
                                  -c "neighbor $NEIGHBOR timers $KEEP_ALIVE_TIMER $HOLD_TIMER" `
                                  -c "exit"
    }

    # Save the configuration
    docker exec $ROUTER vtysh -c "write memory"

    # Restart FRR service to apply changes
    docker exec $ROUTER service frr restart

    # Give some time for the service to restart
    Start-Sleep -Seconds 10

    # Check FRR service status
    docker exec $ROUTER service frr status

    # Show BGP status
    docker exec $ROUTER vtysh -c "show ip bgp summary"
    docker exec $ROUTER vtysh -c "show ip bgp neighbors"
    docker exec $ROUTER vtysh -c "show ip bgp"

    Write-Host "BGP configuration checked for $ROUTER`n"
}

