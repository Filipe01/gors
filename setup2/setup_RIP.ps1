# Define container names and router IPs
$ROUTER_NAMES = @("setup2-router1-1", "setup2-router2-1", "setup2-router3-1", "setup2-router4-1", "setup2-router5-1", "setup2-router6-1",, "setup2-router7-1",, "setup2-router8-1")

$COMPUTER_CONFIGS = @{
    "setup2-computer1-1" = "172.1.0.11";
    "setup2-computer2-1" = "172.2.0.5";
    "setup2-computer3-1" = "172.3.0.4";
    "setup2-computer4-1" = "172.4.0.8"
}

# Set default gateway for each computer
foreach ($COMPUTER in $COMPUTER_CONFIGS.Keys) {
    $GATEWAY_IP = $COMPUTER_CONFIGS[$COMPUTER]
    Write-Host "Setting default gateway for $COMPUTER to $GATEWAY_IP..."

    docker exec $COMPUTER bash -c "ip route delete default"

    # Set the default gateway
    docker exec $COMPUTER bash -c "ip route add default via $GATEWAY_IP"

    Write-Host "Default gateway set for $COMPUTER`n"
}

# Start FRR service on routers and check RIP status
foreach ($ROUTER in $ROUTER_NAMES) {
    Write-Host "Configuring RIP on $ROUTER..."

    # Check FRR service status
    docker exec $ROUTER service frr status

    docker exec $ROUTER vtysh -c "configure terminal" `
                              -c "router rip" `
                              -c "allow-ecmp" 

    # Save the configuration
    docker exec $ROUTER vtysh -c "write memory"

    # Restart FRR service to apply changes
    docker exec $ROUTER service frr restart

    # Enter vtysh and show RIP status and routes
    docker exec $ROUTER vtysh -c "show ip rip status"
    docker exec $ROUTER vtysh -c "show ip route rip"

    Write-Host "RIP configuration checked for $ROUTER`n"
}

Write-Host "All routes and RIP configurations have been set up."
