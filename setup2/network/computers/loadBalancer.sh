#!/bin/bash

# IP addresses of R2 and R3
R7_IP="172.27.0.7"
R8_IP="172.29.0.8"

# IP of the destination PC inside R4
DEST_IP="172.31.0.10"

# Variables to track data sent via R2 and R3
DATA_VIA_R2=0
DATA_VIA_R3=0

# Duration and count settings
DURATION=${1:-10} # Default 10 seconds
COUNT=${2:-100}   # Default 100 iterations
MAX_PARALLEL=${3:-10} # Max parallel iperf processes

# Function to check the route
check_route() {
    traceroute -m 2 $DEST_IP | grep -q "$1"
    echo $?
}

# Function to send data using iperf and update counters
send_data() {
    # Using iperf to send data in background
    iperf -c $DEST_IP -t $DURATION &

    # Introduce a slight delay
    sleep 2

    # Check route and update counters
    if [ $(check_route $R7_IP) -eq 0 ]; then
        ((DATA_VIA_R2++))
    elif [ $(check_route $R8_IP) -eq 0 ]; then
        ((DATA_VIA_R3++))
    fi
}


# Control the number of parallel iperf processes
current_processes=0

# Main loop to send data multiple times
for i in $(seq 1 $COUNT)
do
    send_data
    ((current_processes++))

    if [ "$current_processes" -ge "$MAX_PARALLEL" ]; then
        wait -n
        ((current_processes--))
    fi

    echo "Data sent via R2: $DATA_VIA_R2 times, Data sent via R3: $DATA_VIA_R3 times"
done

# Wait for all remaining background jobs to finish
wait