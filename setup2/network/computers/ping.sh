#!/bin/bash

# IP Address to ping
IP_ADDRESS="172.4.0.10"

# Number of pings
COUNT=100

# Perform the ping and extract the average time
ping -c $COUNT $IP_ADDRESS | tail -1| awk '{print $4}' | cut -d '/' -f 2
