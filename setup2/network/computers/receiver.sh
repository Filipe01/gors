#!/bin/bash

# Port to listen on
PORT=5001

# Using iperf to listen on the specified port
iperf -s -p $PORT &
echo "iperf server listening on port $PORT."

# Keep the script running
while true; do
    sleep 10
done
