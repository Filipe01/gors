#!/bin/bash

# IP address or hostname to traceroute
TARGET="172.4.0.10"

# Number of traceroutes
COUNT=100

# Initialize arrays to store total latencies and counts for each hop
declare -A total_latencies
declare -A hop_counts

for i in $(seq 1 $COUNT); do
    echo "Traceroute $i of $COUNT"
    # Perform traceroute
    traceroute_output=$(traceroute -q 1 $TARGET)

    # Process each line of traceroute output
    while read -r line; do
        # Extract hop number and latency values
        hop=$(echo "$line" | awk '{print $1}')
        latency_values=$(echo "$line" | grep -oP '\d+\.\d+ ms' | sed 's/ ms//')

        if [[ ! -z "$latency_values" ]]; then
            # Calculate the average of the three latency values
            sum_latency=0
            count=0
            for latency in $latency_values; do
                sum_latency=$(echo "$sum_latency + $latency" | bc)
                ((count++))
            done
            if [ $count -gt 0 ]; then
                avg_latency=$(echo "scale=2; $sum_latency / $count" | bc)
                # Accumulate average latencies and counts for each hop
                total_latencies[$hop]=$(echo "${total_latencies[$hop]:-0} + $avg_latency" | bc)
                hop_counts[$hop]=$(( ${hop_counts[$hop]:-0} + 1 ))
            fi
        fi
    done <<< "$(echo "$traceroute_output")"
done

# Calculate and display average latency for each hop
for hop in "${!total_latencies[@]}"; do
    if [ ${hop_counts[$hop]} -gt 0 ]; then
        average_latency=$(echo "scale=2; ${total_latencies[$hop]} / ${hop_counts[$hop]}" | bc)
        echo "Average Latency for Hop $hop: $average_latency ms"
    fi
done
