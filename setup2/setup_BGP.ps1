# Define BGP timer variables
$KEEP_ALIVE_TIMER = 60 # 60s default
$HOLD_TIMER = 180 # 180s default

# Define router names and corresponding BGP neighbor configurations
$BGP_CONFIGS = @{
    "setup2-router1-1" = @{
        "AS" = 65001;
        "Neighbors" = @("172.20.0.2", "172.12.0.6") # Neighbors of Router1
    }
    "setup2-router2-1" = @{
        "AS" = 65002;
        "Neighbors" = @("172.20.0.11", "172.13.0.5", "172.14.0.7", "172.21.0.3") # Neighbors of Router2
    }
    "setup2-router3-1" = @{
        "AS" = 65003;
        "Neighbors" = @("172.21.0.2", "172.15.0.6", "172.16.0.8", "172.22.0.4") # Neighbors of Router3
    }
    "setup2-router4-1" = @{
        "AS" = 65004;
        "Neighbors" = @("172.22.0.3", "172.30.0.7") # Neighbors of Router4
    }
    "setup2-router5-1" = @{
        "AS" = 65005;
        "Neighbors" = @("172.23.0.6", "172.13.0.2") # Neighbors of Router5
    }
    "setup2-router6-1" = @{
        "AS" = 65006;
        "Neighbors" = @("172.23.0.5", "172.12.0.11", "172.15.0.3", "172.24.0.7") # Neighbors of Router6
    }
    "setup2-router7-1" = @{
        "AS" = 65007;
        "Neighbors" = @("172.24.0.6", "172.14.0.2", "172.30.0.4", "172.25.0.8") # Neighbors of Router7
    }
    "setup2-router8-1" = @{
        "AS" = 65008;
        "Neighbors" = @("172.25.0.7", "172.16.0.3") # Neighbors of Router8
    }
}

$COMPUTER_CONFIGS = @{
    "setup2-computer1-1" = "172.1.0.11";
    "setup2-computer2-1" = "172.2.0.5";
    "setup2-computer3-1" = "172.3.0.4";
    "setup2-computer4-1" = "172.4.0.8"
}

# Set default gateway for each computer
foreach ($COMPUTER in $COMPUTER_CONFIGS.Keys) {
    $GATEWAY_IP = $COMPUTER_CONFIGS[$COMPUTER]
    Write-Host "Setting default gateway for $COMPUTER to $GATEWAY_IP..."

    docker exec $COMPUTER bash -c "ip route delete default"

    # Set the default gateway
    docker exec $COMPUTER bash -c "ip route add default via $GATEWAY_IP"

    Write-Host "Default gateway set for $COMPUTER`n"
}

# Configure BGP timers for routers
foreach ($ROUTER in $BGP_CONFIGS.Keys) {
    $BGP_AS = $BGP_CONFIGS[$ROUTER]["AS"]
    $NEIGHBORS = $BGP_CONFIGS[$ROUTER]["Neighbors"]

    Write-Host "Configuring BGP for $ROUTER with AS $BGP_AS..."

    foreach ($NEIGHBOR in $NEIGHBORS) {
        Write-Host "Setting BGP neighbor $NEIGHBOR timers to $KEEP_ALIVE_TIMER $HOLD_TIMER..."
        docker exec $ROUTER vtysh -c "configure terminal" `
                                  -c "router bgp $BGP_AS" `
                                  -c "neighbor $NEIGHBOR timers $KEEP_ALIVE_TIMER $HOLD_TIMER" `
                                  -c "exit"
    }

    # Save the configuration
    docker exec $ROUTER vtysh -c "write memory"

    # Restart FRR service to apply changes
    docker exec $ROUTER service frr restart

    # Give some time for the service to restart
    Start-Sleep -Seconds 10

    # Check FRR service status
    docker exec $ROUTER service frr status

    # Show BGP status
    docker exec $ROUTER vtysh -c "show ip bgp summary"
    docker exec $ROUTER vtysh -c "show ip bgp neighbors"
    docker exec $ROUTER vtysh -c "show ip bgp"

    Write-Host "BGP configuration checked for $ROUTER`n"
}
